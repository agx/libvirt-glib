# Libvirt package strings.
# Copyright (C) 2019 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Mohan Prakash <mpduty@gmail.com>, 2014
# Rajesh Ranjan <rranjan@redhat.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: libvirt-glib 2.0.0\n"
"Report-Msgid-Bugs-To: https://libvirt.org/bugs.html\n"
"POT-Creation-Date: 2019-11-26 12:38+0000\n"
"PO-Revision-Date: 2014-12-17 12:20+0000\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Hindi (http://www.transifex.com/projects/p/libvirt-glib/"
"language/hi/)\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 4.6.2\n"

#: libvirt-gconfig/libvirt-gconfig-helpers.c:141
msgid "No XML document to parse"
msgstr "विश्लेषण करने के लिए कोई XML दस्तावेज़ उपल्बध नहीं "

#: libvirt-gconfig/libvirt-gconfig-helpers.c:149
msgid "Unable to parse configuration"
msgstr "विन्यास के विश्लेषण कर पाने में असमर्थ"

#: libvirt-gconfig/libvirt-gconfig-helpers.c:157
#, c-format
msgid "XML data has no '%s' node"
msgstr "XML data का कोई  '%s' नोड नहीं है"

#: libvirt-gconfig/libvirt-gconfig-object.c:206
msgid "No XML document associated with this config object"
msgstr "इस XML दस्तावेज़ के साथ कोई कॉन्फ़िंग ऑब्जेक्ट नहीं है"

#: libvirt-gconfig/libvirt-gconfig-object.c:214
msgid "No XML schema associated with this config object"
msgstr ""

#: libvirt-gconfig/libvirt-gconfig-object.c:223
#, c-format
msgid "Unable to create RNG parser for %s"
msgstr "%s के लिए RNG विश्लेषक का निर्माण करने में असमर्थ"

#: libvirt-gconfig/libvirt-gconfig-object.c:233
#, c-format
msgid "Unable to parse RNG %s"
msgstr "RNG %s को विश्लेषण करने में असमर्थ"

#: libvirt-gconfig/libvirt-gconfig-object.c:245
#, c-format
msgid "Unable to create RNG validation context %s"
msgstr "RNG अभिप्रमाणित सन्दर्भ %s का निर्माण करने में असमर्थ"

#: libvirt-gconfig/libvirt-gconfig-object.c:255
msgid "Unable to validate doc"
msgstr "डॉक को अभिप्रमाणित करने में असमर्थ"

#: libvirt-gobject/libvirt-gobject-connection.c:438
#, c-format
msgid "Connection %s is already open"
msgstr "कनेक्शन %s पहले से ही खुला है"

#: libvirt-gobject/libvirt-gobject-connection.c:453
#, c-format
msgid "Unable to open %s"
msgstr "%s को खोल पाने में असमर्थ "

#: libvirt-gobject/libvirt-gobject-connection.c:464
msgid "Unable to get connection URI"
msgstr "URI कनेक्शन प्राप्त करने में असमर्थ "

#: libvirt-gobject/libvirt-gobject-connection.c:729
#: libvirt-gobject/libvirt-gobject-connection.c:817
#: libvirt-gobject/libvirt-gobject-connection.c:1504
#: libvirt-gobject/libvirt-gobject-connection.c:1761
msgid "Connection is not open"
msgstr "कनेक्शन खुला नहीं है"

#: libvirt-gobject/libvirt-gobject-connection.c:742
msgid "Failed to fetch list of domains"
msgstr ""

#: libvirt-gobject/libvirt-gobject-connection.c:833
msgid "Failed to fetch list of pools"
msgstr ""

#: libvirt-gobject/libvirt-gobject-connection.c:1034
#: libvirt-gobject/libvirt-gobject-connection.c:1073
msgid "Connection is not opened"
msgstr "कनेक्शन को खोला नहीं गया है"

#: libvirt-gobject/libvirt-gobject-connection.c:1041
msgid "Unable to get hypervisor name"
msgstr "हाइपरविज़र का नाम प्राप्त करने में असमर्थ"

#: libvirt-gobject/libvirt-gobject-connection.c:1079
msgid "Unable to get hypervisor version"
msgstr "हाइपरविज़र का सन्सकरण प्राप्त करने मे असमर्थ"

#: libvirt-gobject/libvirt-gobject-connection.c:1394
#: libvirt-gobject/libvirt-gobject-connection.c:1449
msgid "Failed to create domain"
msgstr "डोमेन का निर्माण कर पाने में असफल"

#: libvirt-gobject/libvirt-gobject-connection.c:1520
msgid "Failed to fetch list of interfaces"
msgstr ""

#: libvirt-gobject/libvirt-gobject-connection.c:1777
msgid "Failed to fetch list of networks"
msgstr ""

#: libvirt-gobject/libvirt-gobject-connection.c:2020
msgid "Failed to create storage pool"
msgstr "संग्रहण पूल का निर्माण करने में असफल"

#: libvirt-gobject/libvirt-gobject-connection.c:2063
msgid "Unable to get node info"
msgstr "नोड की जानकारी प्राप्त करने में असमर्थ"

#: libvirt-gobject/libvirt-gobject-connection.c:2119
msgid "Unable to get domain capabilities"
msgstr ""

#: libvirt-gobject/libvirt-gobject-connection.c:2271
msgid "Unable to get capabilities"
msgstr "सक्षमता प्राप्त करने में असमर्थ"

#: libvirt-gobject/libvirt-gobject-connection.c:2395
msgid "Unable to restore domain"
msgstr "डोमेन पुन: स्थापित करने में असमर्थ"

#: libvirt-gobject/libvirt-gobject-stream.c:347
msgid "virStreamRecv call would block"
msgstr "virStreamRecv का कॉल रोक देता"

#: libvirt-gobject/libvirt-gobject-stream.c:350
#: libvirt-gobject/libvirt-gobject-stream.c:466
#, c-format
msgid "Got virStreamRecv error in %s"
msgstr "%s में virStreamRecv त्रुटि पाया"

#: libvirt-gobject/libvirt-gobject-stream.c:415
msgid "Unable to perform RecvAll"
msgstr "RecvAll के कार्यान्वयन में असमर्थ"

#: libvirt-gobject/libvirt-gobject-stream.c:463
msgid "virStreamSend call would block"
msgstr "virStreamSend का कॉल रोक देता"

#: libvirt-gobject/libvirt-gobject-stream.c:531
msgid "Unable to perform SendAll"
msgstr "SendAll के कार्यान्वयन में असमर्थ"
